import home from '../asstes/home.png'
import search from '../asstes/search.png'
import books from '../asstes/books.png'
import add from '../asstes/add.png'
import heart from '../asstes/heart.png'

const Sidebar = () =>{

    return(
        <div className="Sidebar ">
            <div>
                <img src={home} />
                <p >Início</p>
            </div>
            <div>
                <img src={search} />
                <p >Buscar</p>
            </div>
            <div>
                <img src={books} />
                <p >Sua Livraria</p>
            </div>
            <div className='h-25'>

            </div>
            <div>
                <img src={add} />
                <p >Criar Playlist</p>
            </div>
            <div>
                <img src={heart} />
                <p >Músicas Curtidas</p>
            </div>

        </div>
    )
}

export default Sidebar