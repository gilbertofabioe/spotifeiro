import { Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import Main from './components/Main'
import './App.css';
import FAQ from './components/FAQ';
import Sidebar from './components/Sidebar';


function App() {
  return (
    <div className='principal'>
      <div className="App h-100 row p-0 m-0">
        <div className='col-2 p-0 m-0 sidebar-box'>
          <Sidebar />
        </div>
        <div className='col-10 p-0 m-0 h-100'>
          <Header />
          <Routes>
            <Route path='/' element={<Main />} />
            <Route path='/faq' element={<FAQ />} />
          </Routes>
        </div>
      </div>
      <div className='p-0 m-0 footer-box'>
        <Footer/>
      </div>
    </div>

  );
}

export default App;
